package com.example.tp1;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        Intent intent = getIntent();

        if(intent == null || !intent.hasExtra("animalnom")){
            return;
        }

        String animalstr = intent.getStringExtra("animalnom") ;

        Animal animalObject = AnimalList.getAnimal(animalstr);

        final TextView nom = (TextView) findViewById(R.id.nom);
        nom.setText(animalstr);
        final ImageView img = (ImageView) findViewById(R.id.imageView);
        img.setImageResource(getResources().getIdentifier(animalObject.getImgFile(), "drawable", getPackageName()));
        final TextView esperance = (TextView) findViewById(R.id.tempsesperance);
        esperance.setText(animalObject.getStrHightestLifespan());
        final TextView gestation = (TextView) findViewById(R.id.tempsgestation);
        gestation.setText(animalObject.getStrGestationPeriod());
        final TextView poidnais = (TextView) findViewById(R.id.npoidjeu);
        poidnais.setText(animalObject.getStrBirthWeight());
        final TextView poidadul = (TextView) findViewById(R.id.npoidad);
        poidadul.setText(animalObject.getStrAdultWeight());
        final TextInputEditText vulnerable = (TextInputEditText) findViewById(R.id.vulnerable);
        vulnerable.setText(animalObject.getConservationStatus());
        /*
        nom.getText();
        animalObject.setConservationStatus(); */
    }
}
